var app;

function initPixi() {
    app = new PIXI.Application({width: window.innerWidth, height: window.innerHeight});
    document.body.appendChild(app.view);
    elDisplay = document.getElementById('display').appendChild(app.view);

    
    
    //picture implement
    var image = new PIXI.Sprite.from("pictures/sand.jpg");
    image.width = elDisplay.offsetWidth;
    image.height = elDisplay.offsetHeight;
    app.stage.addChild(image);
    

    //filter implement
    displacementSprite = new PIXI.Sprite.from("/pictures/clouds.jpg");
    displacementFilter = new PIXI.filters.DisplacementFilter(displacementSprite);
    //Wrap mode to repeat to cover entire image
    displacementSprite.texture.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT;
    app.stage.addChild(displacementSprite);
    app.stage.filters = [displacementFilter];

    app.renderer.view.style.transform = 'scale(1.02)';
    displacementSprite.scale.x = 4;
    displacementSprite.scale.y = 4;
    animate();

}

//animate function
function animate() {
    displacementSprite.x += 10;
    displacementSprite.y += 4;
    requestAnimationFrame(animate);
  }
  initPixi();


