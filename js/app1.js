// The application will create a renderer using WebGL, if possible,
// with a fallback to a canvas render. It will also setup the ticker
// and the root stage PIXI.Container
const app = new PIXI.Application({ backgroundColor: 0x1099bb });

// The application will create a canvas element for you that you
// can then insert into the DOM
// var elDisplay = document.getElementsByClassName('display');
console.log(elDisplay)
document.getElementById('display').appendChild(app.view);

// load the texture we need
app.loader.add('turtle', '/pictures/turtle1.png').load((loader, resources) => {
    // This creates a texture from a 'bunny.png' image
    const turtle = new PIXI.Sprite(resources.turtle.texture);

    // Setup the position of the bunny
    // turtle.x = app.renderer.width / 2;
    // turtle.y = app.renderer.height / 2;
    turtle.position.x = 400;
    turtle.position.y = 300;
    turtle.scale.x = 0.2;
    turtle.scale.y = 0.2;

    // Rotate around the center
    turtle.anchor.x = 0.5;
    turtle.anchor.y = 0.5;

    // Add the bunny to the scene we are building
    app.stage.addChild(turtle);

    // Listen for frame updates
    app.ticker.add(() => {
         // each frame we spin the bunny around a bit
        turtle.rotation += 0.001;
    });


// Opt-in to interactivity
turtle.interactive = true;

// Shows hand cursor
turtle.buttonMode = true;

// Pointers normalize touch and mouse
turtle.on('pointerdown', onClick);

// Alternatively, use the mouse & touch events:
// sprite.on('click', onClick); // mouse-only
// sprite.on('tap', onClick); // touch-only

app.stage.addChild(turtle);

function onClick() {
    turtle.scale.x *= 1.25;
    turtle.scale.y *= 1.25;
}

});

/* requestAnimationFrame(animate);
 
function animate() {
    bunny.rotation += 0.01;
 
    renderer.render(stage);
 
    requestAnimationFrame(animate);
} */